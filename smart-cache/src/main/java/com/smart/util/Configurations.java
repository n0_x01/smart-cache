package com.smart.util;

import org.apache.commons.configuration.PropertiesConfiguration;

import java.io.File;
import java.io.InputStream;
import java.io.Reader;
import java.net.URL;

public class Configurations {

    private Configurations() {
    }

    /**
     * 加载属性文件
     * http://commons.apache.org/proper/commons-configuration/userguide_v1.10/howto_properties.html#Properties_files
     * If you do not specify an absolute path, the file will be searched automatically in the following locations:
     * in the current directory
     * in the user home directory
     * in the classpath
     */
    public static PropertiesConfiguration newPropertiesConfiguration(Object obj) {
        PropertiesConfiguration p = new PropertiesConfiguration();
        try {
            if (obj instanceof String) {
                p.load((String) obj);
            } else if (obj instanceof File) {
                p.load((File) obj);
            } else if (obj instanceof URL) {
                p.load((URL) obj);
            } else if (obj instanceof InputStream) {
                p.load((InputStream) obj);
            } else if (obj instanceof Reader) {
                p.load((Reader) obj);
            } else {
                throw new RuntimeException("该方法不支持类型：" + obj.getClass());
            }
        } catch (Exception e) {
            // ignore
            // Logs.msg(Configurations.class, "未找到或加载属性文件！");
        }
        return p;
    }

}
