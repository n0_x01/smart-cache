package com.smart.cache;

import com.smart.jedis.JedisTemplate;
import com.smart.util.Configs;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Timer;
import java.util.TimerTask;

/**
 * @author jacksyen
 * @created 2017-12-27 18:10
 */
public class ClearInvalidCache {

    public static final Logger logger     = LoggerFactory.getLogger(ClearInvalidCache.class);

    private CacheTemplate      cacheTemplate;
    private JedisTemplate      jedisTemplate;
    private CacheSyncHandler   cacheSyncHandler;

    /**
     * 时间间隔(24hour)
     */
    private static final long  PERIOD_DAY = Configs.getLong("redis.cache.clear.interval", 24 * 60 * 60 * 1000L);

    /**
     * 延迟执行（5min)
     */
    private static final long  DELAY      = Configs.getLong("redis.cache.clear.delay", 5 * 60 * 1000L);

    public ClearInvalidCache(final CacheTemplate cacheTemplate) {
        this.cacheTemplate = cacheTemplate;
        this.cacheSyncHandler = new CacheSyncHandler(this.cacheTemplate);
        this.jedisTemplate = cacheTemplate.getJedisTemplate();

        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                cacheTemplate.clearInvalidCache();
            }
        }, DELAY, PERIOD_DAY);
    }

}
